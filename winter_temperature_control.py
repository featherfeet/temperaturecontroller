#!/usr/bin/env python3

"""
Program to control the Christmas lights on the lemon tree using temperature readings in order to prevent frost damage.
"""

import RPi.GPIO as GPIO
import os
import time
import signal
import sys
from time import strftime

from controllabledevice import ControllableDevice
from onewire import TemperatureSensor
from temperaturedatabase import TemperatureDatabase

lemon_tree_lights = ControllableDevice(5, None)

outside_temperature_sensor = TemperatureSensor("/sys/bus/w1/devices/28-000006f2bc11/w1_slave", None)

temperature_database = TemperatureDatabase("/home/pi/TemperatureController/temperature_data.db")

def handler(signal, frame):
    print("\b\bFATAL ERROR: SIGINT detected. EXITING.")
    global lemon_tree_lights
    global outside_temperature_sensor
    global temperature_database
    del lemon_tree_lights
    del outside_temperature_sensor
    del temperature_database
    GPIO.cleanup()
    exit()
signal.signal(signal.SIGINT, handler) # Handle SIGINT (Control-C) nicely.

while True:
    # Delay 10 seconds between readings.
    time.sleep(10)
    # Read the temperature in celsius.
    current_inside_temperature = None
    current_outside_temperature = outside_temperature_sensor.readTemperatureInCelsius()
    # Get the current time in a human-readable format and a machine-readable format.
    current_time = time.time()
    current_time_human_readable = strftime("%m/%d/%Y %H:%M:%S")
    # If it has been more than 24 hours since the last backup, send a backup.
    if current_time - int(open("/home/pi/TemperatureController/last_backup_time.txt").read()) >= 24 * 60 * 60:
        os.system("/home/pi/TemperatureController/backup_database.py")
        with open("/home/pi/TemperatureController/last_backup_time.txt", 'w') as last_backup_time_file:
            last_backup_time_file.write(f"{int(time.time())}")
    # If the temperature read correctly...
    if current_outside_temperature != None:
        # If the temperature is below 3 degrees Celsius...
        if current_outside_temperature <= 3.0:
            # Turn the lights on the lemon tree ON to prevent frost from forming.
            lemon_tree_lights.switchOn()
            lights_state = True
        elif current_outside_temperature >= 5.0:
            # Switch the lights on the lemon tree OFF.
            # Note that this temperature is 2 degrees away from the ON temperature to prevent the state flipping too often.
            lemon_tree_lights.switchOff()
            lights_state = False
    # If the temperature did not read correctly, then skip the logging for this point in time.
    else:
        continue
    # Log the temperature and lighting data.
    temperature_database.logDataPoint(current_time, current_inside_temperature, current_outside_temperature, lights_state, False)
    print("INFO: Time is '{}'. Temperature is {} degrees Celsius and lights are {}.".format(current_time_human_readable, current_outside_temperature, ("off", "on")[lights_state]))
