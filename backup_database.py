#!/usr/bin/env python3

"""
Program to take a copy of the temperature database and copy it to a backup server. This program uses the Sqlite3 backup API so that it can run safely while the database is being accessed.
"""

import time
import sqlite3
import os

conn = sqlite3.connect("/home/pi/TemperatureController/temperature_data.db")

backup_filename = "/home/pi/TemperatureController/temperature_data_backup_{}.db".format(int(time.time()))
backup_conn = sqlite3.connect(backup_filename)

def progressHandler(status, remaining, total):
	print("Copied {} of {} pages ({}% complete).".format(total - remaining, total, int(((total - remaining) / total) * 100.0)))

print("Starting database backup...")
conn.backup(backup_conn, pages = 100, progress = progressHandler)
backup_conn.commit()
backup_conn.close()
conn.close()
print("Database backup complete.")

print("Starting to copy database backup to remote location...")
return_value = os.system("scp \"{}\" oliver@closet4u:/mnt/Backup/TemperatureController_Database_Backups/".format(backup_filename))
print("Database backup copy complete.")

# If the backup successfully copied to the remote machine, delete it here.
if return_value == 0:
	os.system("rm \"{}\"".format(backup_filename))
else:
	print("The backup failed to copy to the remote location!")
