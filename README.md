# TemperatureController

Example `/etc/supervisor/conf.d/service.conf` file for Supervisor to run this:

```
[program:temperaturecontroller]
command=sh /home/pi/TemperatureController/temperaturecontroller.sh
directory=/home/pi/TemperatureController
log_stdout=true
log_stderr=true
autostart=true
autorestart=true
startsecs=10
stopwaitsecs=30
killasgroup=true
priority=500
```
