#!/usr/bin/env zsh

echo "This script must be run from the directory that it is in (the directory directly above raw_data). Press ENTER if this is true."
read

cd raw_data
for file in *;
    ../fixer.py "$file" "$file.csv"
