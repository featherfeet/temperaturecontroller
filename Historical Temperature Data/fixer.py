#!/usr/bin/env pypy3

import os
import sys
import re
import readline
from datetime import datetime

if len(sys.argv) != 3:
    print("Usage: ./fixer.py inputfilename outputfilename")
    print("Takes a corrupted CSV and removes all lines that don't look like 4-column CSV data.")

input_file_name = sys.argv[1]
output_file_name = sys.argv[2]

os.system("strings \"{}\" > \"{}.ascii\"".format(input_file_name, input_file_name))
os.system("mv \"{}.ascii\" \"{}\"".format(input_file_name, input_file_name))

input_file = open(input_file_name, 'r')
output_file = open(output_file_name, 'w')

data = input_file.read()
data = data.split('\n')

csv_line_regex = re.compile("\d\d/\d\d/\d\d\d\d \d\d:\d\d:\d\d,(?:-|)\d*\.\d*,(?:-|)\d*\.\d*,(on|off)")

output_file.write("TIME,INSIDE_TEMPERATURE,OUTSIDE_TEMPERATURE,LIGHTS_STATE\n\n")

for i, line in enumerate(data):
    match = csv_line_regex.match(line)
    if match != None:
        matching_part = match.group(0)
        pieces = matching_part.split(',')
        try:
            timestamp = datetime.strptime(pieces[0], "%m/%d/%Y %H:%M:%S")
            if timestamp.year < 2015 or timestamp.year > 2018:
                print(timestamp.year)
        except ValueError:
            print("Datetime invalid.")
            print("Previous line: {}".format(data[i - 1]))
            print("Next line: {}".format(data[i + 1]))
            print("Correct this line: {}".format(line))
            line = input("> ")
            matching_part = line
        output_file.write(matching_part + '\n')

output_file.close()
input_file.close()
