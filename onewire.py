import RPi.GPIO as GPIO
import os
from time import strftime

first_time = True

def initializeOneWireBus():
    # Check that this Raspberry Pi is configured for the One-Wire Bus.
    configFileHandle = open("/boot/firmware/config.txt", "r")
    configFileContents = configFileHandle.read()
    configFileHandle.close()
    if "dtoverlay=w1-gpio" in configFileContents:
        print("INFO: This Raspberry Pi has 1-Wire temperature sensing enabled. If this still doesn't work, rebooting may be necessary.")
    else:
        print("FATAL ERROR: This Raspberry Pi's config.txt does not have 1-Wire temperature sensing enabled. Please add the following line to the end of /boot/config.txt: 'dtoverlay=w1-gpio' EXITING")
        GPIO.cleanup()
        exit()
    # Load the OneWire Bus GPIO kernel module.
    print("INFO: Executing the following command: `sudo modprobe w1-gpio`")
    firstCommandReturnValue = os.system("sudo modprobe w1-gpio")
    if firstCommandReturnValue == 0:
        print("INFO: The command returned 0 (success).")
    else:
        print("FATAL ERROR: The command did not return 0 (failure). EXITING")
        GPIO.cleanup()
        exit()
    # Load the OneWire Bus temperature sensor module.
    print("INFO: Executing the following command: `sudo modprobe w1-therm`")
    secondCommandReturnValue = os.system("sudo modprobe w1-therm")
    if secondCommandReturnValue == 0:
        print("INFO: The command returned 0 (success).")
    else:
        print("FATAL ERROR: The command did not return 0 (failure). EXITING")
        GPIO.cleanup()
        exit()

class TemperatureSensor:
    """
    Class for accessing the DS18B20 One-Wire temperature sensor.
    """
    
    def __init__(self, path, state_file_name):
        """
        Instantiate a new DS18B20 temperature sensor.
        Parameters:
        path (str): The absolute path to the w1_slave file of the sensor. Usually something like "/sys/bus/w1/devices/xx-xxxxxxxxxxxx/w1_slave".
        """
        global first_time
        if first_time:
            initializeOneWireBus()
            first_time = False
        self.path = path
        self.state_file_name = state_file_name

    def readTemperatureInCelsius(self):
        """
        Read out the temperature in degrees Celsius. Returns the temperature as a floating-point number in celsius.
        """
        # Read the raw sensor info.
        temp_file = open(self.path, 'r')
        temp_file_contents = temp_file.read()
        temp_file.close()
        # If the sensor got a successful reading:
        if "YES" in temp_file_contents:
            temp_string = temp_file_contents[temp_file_contents.find('t')+2:]
            temp = float(temp_string) / 1000.0
            if self.state_file_name:
                state_file = open(self.state_file_name, 'w') # Temperature file to be uploaded to the website.
                state_file.write(str(temp))        # Temperature file to be uploaded to the website.
                state_file.flush()
                state_file.close()                 # Temperature file to be uploaded to the website.
            return temp
        else:
            print("NON-FATAL ERROR: Temperature on outside sensor is not readable. RETURNING NONE.")
            return None
