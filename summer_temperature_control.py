#!/usr/bin/env python3

"""
Program to control the chicken mister to cool them down in the summer.
"""

import RPi.GPIO as GPIO
import os
import time
import signal
import sys
from time import strftime

from controllabledevice import ControllableDevice
from onewire import TemperatureSensor
from temperaturedatabase import TemperatureDatabase

mister = ControllableDevice(5, None)

outside_temperature_sensor = TemperatureSensor("/sys/bus/w1/devices/28-000006f2bc11/w1_slave", None)

temperature_database = TemperatureDatabase("/home/pi/TemperatureController/temperature_data.db")

def handler(signal, frame):
    print("\b\bFATAL ERROR: SIGINT detected. EXITING.")
    global mister
    global outside_temperature_sensor
    global temperature_database
    del mister
    del outside_temperature_sensor
    del temperature_database
    GPIO.cleanup()
    exit()
signal.signal(signal.SIGINT, handler) # Handle SIGINT (Control-C) nicely.

while True:
    # Delay 10 seconds between readings.
    time.sleep(10)
    # Read the temperature in celsius.
    current_inside_temperature = None
    current_outside_temperature = outside_temperature_sensor.readTemperatureInCelsius()
    # Get the current time in a human-readable format and a machine-readable format.
    current_time = time.time()
    current_time_human_readable = strftime("%m/%d/%Y %H:%M:%S")
    # If the temperature read correctly...
    if current_outside_temperature != None:
        # If the temperature is above 33.33 degrees Celsius...
        if current_outside_temperature >= 33.3: # 92 degrees Fahrenheit
            # Turn the mister ON to cool the chickens.
            mister.switchOn()
            mister_state = True
        elif current_outside_temperature <= 31.3:
            # Switch the mister OFF.
            # Note that this temperature is 2 degrees away from the ON temperature to prevent the state flipping too often.
            mister.switchOff()
            mister_state = False
    # If the temperature did not read correctly, then skip the logging for this point in time.
    else:
        continue
    # Log the temperature and mister data.
    temperature_database.logDataPoint(current_time, current_inside_temperature, current_outside_temperature, False, mister_state)
    print("INFO: Time is '{}'. Temperature is {} degrees Celsius and mister is {}.".format(current_time_human_readable, current_outside_temperature, ("off", "on")[mister_state]))
