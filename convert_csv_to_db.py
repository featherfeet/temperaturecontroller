#!/usr/bin/env pypy3

"""
Program to convert a temperature CSV from the old temperature controller program into a Sqlite3 database for the new temperature controller program.
"""

import sys
import csv
import time
from datetime import datetime
from temperaturedatabase import *

if len(sys.argv) != 3:
	print("Usage: ./convert_csv_to_db.py LOGFILE.csv temperature_data.db")
	print("This program takes a CSV from the old temperature controller code and adds its data to the end of a Sqlite3 database used by the new temperature controller code. It is advisable to only use this with an empty Sqlite3 database output.")

csv_file_name = sys.argv[1]
database_file_name = sys.argv[2]

temperature_database = TemperatureDatabase(database_file_name, automatic_commit = False)

print("Scanning file to count number of data points (this make take a while)...")
lines = open(csv_file_name).read().split('\n')
total_lines = len(lines)
print("{} data points to process.".format(total_lines - 2))

start_time = time.time()

if "TIME,INSIDE_TEMPERATURE,OUTSIDE_TEMPERATURE,LIGHTS_STATE" in lines:
	csv_type = 1
elif "Timestamp,Outside Temperature,Lighting State" in lines:
	csv_type = 2

for i, row in enumerate(csv.reader(open(csv_file_name))):
	# Every 100 lines, print out status.
	if i % 100 == 0:
		print("\r{}%".format(int((i / total_lines) * 100.0)), end = '') # Print out percentage completed.
		sys.stdout.flush()
	if csv_type == 1:
		# If it's the "title" row or an empty line, don't process it.
		if i == 0 or len(row) != 4:
			continue
		else:
			timestamp_raw = row[0]
			inside_temperature_raw = row[1]
			outside_temperature_raw = row[2]
			lights_state_raw = row[3]

			timestamp = datetime.strptime(timestamp_raw, "%m/%d/%Y %H:%M:%S")
			inside_temperature = float(inside_temperature_raw)
			outside_temperature = float(outside_temperature_raw)
			if lights_state_raw == "on":
				lights_state = True
			elif lights_state_raw == "off":
				lights_state = False

			temperature_database.logDataPoint(timestamp.timestamp(), inside_temperature, outside_temperature, lights_state)
	elif csv_type == 2:
		# If it's the "title" row or an empty line, don't process it.
		if i == 0 or len(row) != 3:
			continue
		else:
			timestamp_raw = row[0]
			outside_temperature_raw = row[1]
			lights_state_raw = row[2]

			timestamp = datetime.strptime(timestamp_raw, "%m/%d/%Y %H:%M:%S")
			outside_temperature = float(outside_temperature_raw)
			if lights_state_raw == "on":
				lights_state = True
			elif lights_state_raw == "off":
				lights_state = False

			temperature_database.logDataPoint(timestamp.timestamp(), None, outside_temperature, lights_state)

temperature_database.commit()
del temperature_database

print("\nConversion complete! {} rows processed in {} seconds.".format(total_lines, time.time() - start_time))
