import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

class ControllableDevice:
    """
    Class that represents a generic on/off device (usually controlled by an electromechanical relay).
    """

    def __init__(self, pin, state_file_name, on_state = GPIO.HIGH, off_state = GPIO.LOW):
        """
        Instantiate a new controllable device.
        Parameters:
        pin (int): The GPIO pin (in the GPIO.BCM layout) that the device or its relay is connected to.
        state_file_name (str or None): The path to a file where the current state of the device ("on" or "off") will be written. This is so that other programs can read the current state of the device easily.
        on_state (int): The pin state used to switch the device on. Defaults to GPIO.HIGH.
        off_state (int): The pin state used to switch the device off. Defaults to GPIO.LOW.
        """
        self.pin = pin
        self.state_file_name = state_file_name 
        self.on_state = on_state
        self.off_state = off_state
        GPIO.setup(pin, GPIO.OUT, initial = off_state)

    def switchOn(self):
        '''Switches the device on and writes "on" (without quotes) to the file specified in state_file_name.'''
        print(f"Switching pin {self.pin} to state {self.on_state} (on state).")
        GPIO.setup(self.pin, GPIO.OUT)
        GPIO.output(self.pin, self.on_state)
        if self.state_file_name:
            state_file = open(self.state_file_name, 'w')
            state_file.write("on")
            state_file.close()

    def switchOff(self):
        '''Switches the device off and writes "off" (without quotes) to the file specified in state_file_name.'''
        print(f"Switching pin {self.pin} to state {self.off_state} (off state).")
        GPIO.setup(self.pin, GPIO.OUT)
        GPIO.output(self.pin, self.off_state)
        if self.state_file_name:
            state_file = open(self.state_file_name, 'w')
            state_file.write("off")
            state_file.close()
