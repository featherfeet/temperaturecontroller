#!/usr/local/bin/python3.8

"""
Program to generate a Matplotlib graph of the temperature in the temperature database.
"""

import time
import argparse
import matplotlib.pyplot as plt
from temperaturedatabase import *

# Parse command-line arguments.
parser = argparse.ArgumentParser()
parser.add_argument("--last-n-hours", help = "Graph only data for the last _n_ hours.")
parser.add_argument("--database", help = "Specify a different path to the Sqlite3 database file.")
args = parser.parse_args()

if args.database == None:
    args.database = "./temperature_data.db"

# Open the database.
temperature_database = TemperatureDatabase(args.database)

# Retrieve data.
now = time.time()
if args.last_n_hours != None:
	n_hours_ago = now - float(args.last_n_hours) * 60.0 * 60.0
	temperature_data_points = temperature_database.readDataBetweenTimes(n_hours_ago, now)
else:
	temperature_data_points = temperature_database.readAllData()
	start_time = temperature_data_points[0].time_seconds

# Plan where to draw vertical lines on the graph to indicate "this time yesterday," "this time the day before yesterday," etc.
vertical_line_locations = []
vertical_line_location = now
while True:
	vertical_line_location -= 24 * 60 * 60
	if args.last_n_hours != None and vertical_line_location < n_hours_ago:
		break
	elif args.last_n_hours == None and vertical_line_location < start_time:
		break
	vertical_line_locations.append(datetime.fromtimestamp(vertical_line_location))

# Graph the data.
print("Graphing {} data points.".format(len(temperature_data_points)))

timestamps = [data_point.datetime for data_point in temperature_data_points]
#inside_temperatures = [data_point.inside_temperature_celsius for data_point in temperature_data_points]
outside_temperatures = [data_point.outside_temperature_celsius for data_point in temperature_data_points]
colors = [([1, 0, 0], [0, 0, 1])[data_point.lights_state] for data_point in temperature_data_points]

plt.plot(timestamps, outside_temperatures, "-k", zorder = 0)
plt.scatter(timestamps, outside_temperatures, color = colors, zorder = 1, s = 5)
plt.xlim(timestamps[0], timestamps[-1])
plt.ylim(-5, 45)
for vertical_line_location in vertical_line_locations:
	plt.axvline(x = vertical_line_location, color = 'k', linestyle = "--")
plt.xlabel("Timestamp")
plt.ylabel(u"Temperature (\N{DEGREE SIGN}C)")
plt.show()
