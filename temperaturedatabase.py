from datetime import datetime
import sqlite3

class TemperatureDataPoint:
	"""
	Class that represents one point in time with temperature and lighting data.
	"""

	def __init__(self, index, time_seconds, inside_temperature_celsius, outside_temperature_celsius, lights_state, mister_state):
		"""
		Create a new TemperatureDataPoint.
		Parameters:
		index (int): The id in the database of this data point.
		time_seconds (float): The timestamp of the data point (in seconds since the Unix epoch).
		inside_temperature_celsius (float): The temperature in degrees Celsius inside the chicken coop.
		outside_temperature_celsius (float): The temperature in degrees Celsius outside the chicken coop.
		lights_state (bool): True if the lights were on, False if they were not.
		mister_state (bool): True if the mister was on, False if it was not.
		"""
		self.index = index
		self.time_seconds = time_seconds
		self.datetime = datetime.fromtimestamp(self.time_seconds)
		self.inside_temperature_celsius = inside_temperature_celsius
		self.outside_temperature_celsius = outside_temperature_celsius
		self.lights_state = lights_state
		self.mister_state = mister_state

	def __str__(self):
		"""
		Create a human-readable string representing this data point.
		"""
		return "TemperatureDataPoint(index = {})".format(self.index)

	def __repr__(self):
		"""
		Create a human-readable string representing this data point.
		"""
		return str(self)

class TemperatureDatabase:
	"""
	Class that wraps a Sqlite3 database as a means of logging temperature data.
	"""

	def __init__(self, database_file_path, automatic_commit = True):
		"""
		Initialize the database.
		Parameters:
		database_file_path (str): The path to the Sqlite3 database file to use.
		automatic_commit (bool): Whether to automatically commit changes to the database after every call to logDataPoint. If this is False, you have to call the commit function whenever you want to save changes. Defaults to True.
		"""
		self.conn = sqlite3.connect(database_file_path)
		self.cursor = self.conn.cursor()
		self.cursor.execute("CREATE TABLE IF NOT EXISTS temperature_data (id INTEGER PRIMARY KEY AUTOINCREMENT, time_seconds REAL, inside_temperature_celsius REAL, outside_temperature_celsius REAL, lights_state INTEGER, mister_state INTEGER)")
		self.conn.commit()
		self.automatic_commit = automatic_commit

	def logDataPoint(self, time_seconds, inside_temperature_celsius, outside_temperature_celsius, lights_state, mister_state):
		"""
		Log a data point in the database.
		Parameters:
		time_seconds (float): The number of seconds since the Unix epoch. Retrieve this value using time.time().
		inside_temperature_celsius (float): The temperature in degrees Celsius inside the chicken coop.
		outside_temperature_celsius (float): The temperature in degrees Celsius outside the chicken coop.
		lights_state (bool): True if the lights were on, False if they were not.
		mister_state (bool): True if the mister was on, False if it was not.
		"""
		insert_values = (time_seconds, inside_temperature_celsius, outside_temperature_celsius, int(lights_state), int(mister_state))
		self.cursor.execute("INSERT INTO temperature_data (time_seconds, inside_temperature_celsius, outside_temperature_celsius, lights_state, mister_state) VALUES (?, ?, ?, ?, ?)", insert_values)
		if self.automatic_commit:
			self.conn.commit()

	def readDataBetweenTimes(self, start_time, end_time):
		"""
		Retrieve all data points between two times.
		Parameters:
		start_time (float): The start of the interval to retrieve (in seconds since the Unix epoch).
		end_time (float): The end of the interval to retrieve (in seconds since the Unix epoch).
		"""
		time_interval = (start_time, end_time)
		data_points = self.cursor.execute("SELECT id, time_seconds, inside_temperature_celsius, outside_temperature_celsius, lights_state, mister_state FROM temperature_data WHERE time_seconds BETWEEN ? AND ? ORDER BY time_seconds ASC", time_interval)
		data_points_objects = []
		for row in data_points:
			data_points_objects.append(TemperatureDataPoint(row[0], row[1], row[2], row[3], bool(row[4]), bool(row[5])))
		return data_points_objects

	def readAllData(self):
		"""
		Retrieve all data points.
		"""
		data_points = self.cursor.execute("SELECT id, time_seconds, inside_temperature_celsius, outside_temperature_celsius, lights_state, mister_state FROM temperature_data ORDER BY time_seconds ASC")
		data_points_objects = []
		for row in data_points:
			data_points_objects.append(TemperatureDataPoint(row[0], row[1], row[2], row[3], bool(row[4]), bool(row[5])))
		return data_points_objects

	def commit(self):
		self.conn.commit()

	def saveBackupFile(self, backup_file_name):
		self.conn.commit()
		backup_connection = sqlite3.connect(backup_file_name)
		self.conn.backup(backup_connection)
		backup_connection.close()

	def __del__(self):
		"""
		Destroy the database connection.
		"""
		self.conn.commit()
		self.conn.close()
