#!/usr/bin/env python3

import re
import os
from datetime import datetime

extractor_regex = re.compile("temperature_data_backup_(\d+).db")

file_names = os.listdir("/mnt/Backup/TemperatureController_Database_Backups/")

file_names_by_timestamp = {}
timestamps = []
for file_name in file_names:
    match = extractor_regex.match(file_name)
    if match != None:
        timestamp = datetime.fromtimestamp(int(match.group(1)))
        file_names_by_timestamp[timestamp] = file_name
        timestamps.append(timestamp)

print("Backups found:")
for timestamp in sorted(timestamps):
    print(f"\t* {timestamp.strftime('%A, %B %e, %Y at %l:%M %p')} - {file_names_by_timestamp[timestamp]}")
